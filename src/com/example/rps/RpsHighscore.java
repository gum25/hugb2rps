package com.example.rps;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class RpsHighscore extends Activity {

	private int draw =0;
	private int userScore =0;
	private int compScore =0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rps_highscore);
		Bundle score = getIntent().getExtras();
		
		
		if (score != null)
		{
			draw = score.getInt("Draw");
			userScore = score.getInt("Win");
			compScore = score.getInt("Lost");
			
			System.out.println(draw);
			//TextView drawscore = (TextView)findViewById(R.id.pointsdraw);
			//drawscore.setText(draw);
		}

		//TextView drawscore = (TextView)findViewById(R.id.pointsdraw);
		TextView winscore = (TextView)findViewById(R.id.pointswin);
		winscore.setText("Wins = " + String.valueOf(userScore));
		TextView losescore = (TextView)findViewById(R.id.pointslose);
		losescore.setText("Losses = " + String.valueOf(compScore));
		TextView drawscore = (TextView)findViewById(R.id.pointsdraw);
		drawscore.setText("Draws = " + String.valueOf(draw));
		
	}
	
	
	/*public void onClickPoints(View v){
		
		TextView drawscore = (TextView)findViewById(R.id.pointsdraw);
		drawscore.setText("Jafntefli " + String.valueOf(draw));
	}*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rps_highscore, menu);
		return true;
	}
	
	@Override
	   public boolean onOptionsItemSelected(MenuItem item)
	   {  
	       switch (item.getItemId())
	       {
	       	case R.id.menu_play:
	       	Intent intent1 = new Intent(this, Play.class);
	       	startActivity(intent1);
	       	return true;
	       	
	       	case R.id.menu_rps:
	       	Intent intent2 = new Intent(this, RockPaperScissors.class);
	       	startActivity(intent2);
	       	return true;
	       }
			return true;
	   }

}
