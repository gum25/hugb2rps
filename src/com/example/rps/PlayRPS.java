package com.example.rps;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class PlayRPS extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_rps);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play_r, menu);
		return true;
	}
	
	public void onClickLocal(View v){
		Intent i = new Intent(getApplicationContext(), RockPaperScissors.class);
		startActivity(i);
	}
	
	public void onClickOnline(View v){
		Intent i = new Intent(getApplicationContext(), RockPaperScissorsOnline.class);
		startActivity(i);
	}

}
