package com.example.rps;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class Play extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onClickRPS(View v){
		Intent i = new Intent(getApplicationContext(), PlayRPS.class);
		startActivity(i);
	}
	
	public void onClickYatzy(View v){
		Intent i = new Intent(getApplicationContext(), PlayYatzy.class);
		startActivity(i);
	}

}
