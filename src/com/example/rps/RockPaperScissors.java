package com.example.rps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
 
public class RockPaperScissors extends Activity implements OnClickListener {
  
 public enum Option {
  ROCK, PAPER, SCISSORS
 }
  
 public enum Result {
  WIN, LOSS, DRAW  
 }
  
 private Option userSelection;
 private Result gameResult;
 
 private int userScore = 0;
 private int compScore = 0;
 private int draw = 0;
  
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rock_paper_scissors);
         
        ImageButton buttonRock = (ImageButton) findViewById(R.id.buttonRock);
        ImageButton buttonpaper = (ImageButton) findViewById(R.id.buttonPaper);
        ImageButton buttonScissors = (ImageButton) findViewById(R.id.buttonScissors);
         
        // Set click listening event for all buttons.
        buttonRock.setOnClickListener(this);
        buttonpaper.setOnClickListener(this);
        buttonScissors.setOnClickListener(this);
    }
    
    public void onClickScore(View v){
		Intent i = new Intent(getApplicationContext(), RpsHighscore.class);
		i.putExtra("Draw", draw);
		i.putExtra("Win", userScore);
		i.putExtra("Lost", compScore);
		startActivity(i);
		System.out.println("Draw " + draw);
		System.out.println("Comp " + compScore);
		System.out.println("User " + userScore);
	}
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rock_paper_scissors, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {  
        switch (item.getItemId())
        {
        	case R.id.menu_play:
        	Intent intent1 = new Intent(this, Play.class);
        	startActivity(intent1);
        	return true;
        	
        	case R.id.menu_rpsHighScore:
        	Intent i = new Intent(this, RpsHighscore.class);
        	startActivity(i);
        	return true;
        }
 		return true;
    }
 
 @Override
 public void onClick(View v) {
   

  boolean play = true;
   
  switch (v.getId()) {
   case R.id.buttonRock:
    userSelection = Option.ROCK;
    TextView rocktext = (TextView)findViewById(R.id.myPick);
    rocktext.setText("You picked ROCK");	
    break; 
   case R.id.buttonPaper:
    userSelection = Option.PAPER;
    TextView papertext = (TextView)findViewById(R.id.myPick);
    papertext.setText("You picked PAPER");	
    break;
   case R.id.buttonScissors:
    userSelection = Option.SCISSORS;
    TextView scissorstext = (TextView)findViewById(R.id.myPick);
    scissorstext.setText("You picked SCISSORS");
    break;
  }
   
  if(play) {
   play();
   showResults();
  }
 }
 
 public void showResults() 
 {
	 
     if(gameResult == Result.LOSS) 
     {
    	 TextView losstext = (TextView)findViewById(R.id.winner);
    	 losstext.setText("You Loose");	
    	 compScore++;
     } 
     else if(gameResult == Result.WIN) 
     {
         TextView wintext = (TextView)findViewById(R.id.winner);
       	 wintext.setText("You Win");
       	 userScore++;
     } 
     else if(gameResult == Result.DRAW) 
     {
         TextView drawtext = (TextView)findViewById(R.id.winner);
       	 drawtext.setText("It's a draw");
       	 draw = draw +1;
     } 
} 

 private void play() {
  // Generates a random play.
  int rand =  ((int)(Math.random() * 10)) % 3;
  Option androidSelection = null;
   
  // Sets the right image according to random selection.
  switch (rand) {
   case 0:
    androidSelection = Option.ROCK;
    TextView rocktext = (TextView)findViewById(R.id.opponentPick);
    rocktext.setText("Opponent picked ROCK");	 
    break;
   case 1:
    androidSelection = Option.PAPER;
    TextView papertext = (TextView)findViewById(R.id.opponentPick);
    papertext.setText("Opponent picked PAPER");	
    break;
   case 2:
    androidSelection = Option.SCISSORS;
    TextView scissorstext = (TextView)findViewById(R.id.opponentPick);
    scissorstext.setText("Opponent picked SCISSORS");	
    break;
  }
  // Determine game result according to user selection and Android selection.
  if(androidSelection == userSelection) {
   gameResult = Result.DRAW;
  }
  else if(androidSelection == Option.ROCK && userSelection == Option.SCISSORS) {
   gameResult = Result.LOSS;
  }
  else if(androidSelection == Option.PAPER && userSelection == Option.ROCK) {
   gameResult = Result.LOSS;
  } 
  else if(androidSelection == Option.SCISSORS && userSelection == Option.PAPER) {
   gameResult = Result.LOSS;
  } else {
   gameResult = Result.WIN;
  }  
 }    
}
